#==========================================================================================================================================
## Install NerdFont - Roboto Mono

# Go to here and download the Roboto Mono zip package
https://github.com/ryanoasis/nerd-fonts/releases

# Double click and install
"Roboto Mono Nerd Font Complete Windows Compatible.ttf"


#==========================================================================================================================================
## Edit AutoHotKey Scripts in VSCode

# Edit registry value
Computer\HKEY_CLASSES_ROOT\AutoHotkeyScript\Shell\Edit\Command

# Set value to
"C:\Users\jeff\AppData\Local\Programs\Microsoft VS Code\Code.exe" "%1"

# Exit AHK scripts and re-open them

#==========================================================================================================================================
## VSCode 

## Install Theme - SynthWave '84
# enable glow
Ctrl+Shift+P -> "Enable Neon Dreams"
# Settings.json:
"synthwave84.brightness": "1.00",

# Set font settings
"editor.fontSize": 18,
"editor.fontFamily": "'RobotoMono NF', Consolas, 'Courier New', monospace",
