#==========================================================================================================================================
## Setup username and email on Git
git config --global user.name  "JeffReeves"
git config --global user.email "jeff@binary.run"


#==========================================================================================================================================
## Delete Zone.Identifier files
find /share -type f -name "*Zone.Identifier" -print0 | xargs -i{} -0 rm {}


#==========================================================================================================================================
## Setup a new Ubuntu 20.04 - Nginx, PHP, MariaDB stack

# update system
sudo apt-get update && sudo apt-get dist-upgrade -y && sudo apt-get autoremove -y

# install Nginx
sudo apt-get install nginx

# install MariaDB
sudo apt-get install mariadb-server

# install PHP and PHP-FPM
sudo apt-get install php7.4 php7.4-common php7.4-fpm php7.4-mysql php7.4-xml php7.4-xmlrpc php7.4-curl php7.4-gd php7.4-imagick php7.4-cli php7.4-dev php7.4-imap php7.4-mbstring php7.4-opcache php7.4-soap php7.4-zip php7.4-intl php7.4-tidy

# install Python 3.8
sudo apt-get install python3.8

# install Nodejs
sudo apt-get install nodejs

# install VIM, git, and Ansible
sudo apt-get install vim git ansible


# configure username and email for git


# configure PHP


# configure MariaDB

# import backups


# configure Nginx

# generate / install SSL